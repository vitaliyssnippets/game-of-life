import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	private static char[][] map;
	private static String mapFileName;
	private static ParameterParser parser;
	private static Game game;
	private static int turns = -1;
	private static int rate = 100;
	
	public static void main(String[] args) {
		initialize(args);
	}
	
	private static void initialize(String args[]) {
		loadParams(args);
		loadMap();
		loadGame();
		playGame(turns);
	}
	
	private static void playGame(int turns) {
		boolean useWaiting = rate > 0;
		if(turns == -1) {
			int i = 1;
			while(true) {
				println("Turn " + i + ":");
				game.printMap();
				game.step();
				i++;
				if(useWaiting) try {
					Thread.sleep(rate);
				} catch (Exception e) {}
			}
		} else {
			for(int i = 1; i <= turns; i++) {
				println("Turn " + i + ":");
				game.printMap();
				game.step();
				if(useWaiting) try {
					Thread.sleep(rate);
				} catch (Exception e) {}
			}
		}
	}
	
	private static void loadParams(String args[]) {
		parser = new ParameterParser(args);
		mapFileName = parser.getArgAtIndex(0);
		String turnsString = parser.getOptionValue("turns");
		if(turnsString != null) turns = Integer.parseInt(turnsString);
		String rateString = parser.getOptionValue("rate");
		if(rateString != null) rate = Integer.parseInt(rateString);
	}
	
	private static void loadMap() {
		File mapFile = new File(mapFileName);
		Scanner sc;
		try {
			sc = new Scanner(mapFile);
		} catch (FileNotFoundException e) {
			printError(e.getMessage());
			return;
		}
		String firstLine = sc.nextLine();
		ArrayList<String> lst = new ArrayList<String>();
		lst.add(firstLine);
		while(sc.hasNextLine()) {
			lst.add(sc.nextLine());
		}
		sc.close();	
		map = new char[lst.size()][firstLine.length()];
		for(int i = 0; i < lst.size(); i++) {
			map[i] = lst.get(i).toCharArray();
		}
	}
	
	private static void loadGame() {
		game = new Game(map);
	}
	
	public static void println(String ln) {
		System.out.println(ln);
	}
	
	public static void printError(String ln) {
		println("ERROR: " + ln);
	}

}
