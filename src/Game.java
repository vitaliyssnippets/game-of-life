
public class Game {

	private static char[][] map;
	private static int rows, cols;
	private static int lastRowIndex, lastColIndex;
	private static char aliveChar = '@';
	private static char deadChar = 'O';
	private static char aliveDisplayChar = '@';
	private static char deadDisplayChar = 'O';
	
	public Game(char[][] map) {
		this.map = map;
		rows = map.length;
		cols = map[0].length;
		lastRowIndex = rows - 1;
		lastColIndex = cols - 1;
	}
	
	public int getRows() {
		return rows;
	}
	
	public int getCols() {
		return cols;
	}
	
	public void printMap() {
		for(int i = 0; i < rows; i++) {
			String line = "";
			for(int j = 0; j < cols; j++) {
				char c = map[i][j];
				if(isAlive(c)) c = aliveDisplayChar;
				else c = deadDisplayChar;
				line += c;
			}
			System.out.println(line);
		}
	}
	
	public void step() {
		char[][] newMap = new char[rows][cols];
		for(int i = 0; i < rows; i++) {
			for(int j = 0; j < cols; j++) {
					int aliveNeighbours = 0;
					if(isAlive(getTopLeftOf(j, i))) aliveNeighbours++;
					if(isAlive(getTopOf(j, i))) aliveNeighbours++;
					if(isAlive(getTopRightOf(j, i))) aliveNeighbours++;
					if(isAlive(getRightOf(j, i))) aliveNeighbours++;
					if(isAlive(getBotRightOf(j, i))) aliveNeighbours++;
					if(isAlive(getBotOf(j, i))) aliveNeighbours++;
					if(isAlive(getBotLeftOf(j, i))) aliveNeighbours++;
					if(isAlive(getLeftOf(j, i))) aliveNeighbours++;	
					if(isAlive(map[i][j])) {
						if(aliveNeighbours < 2) {
							newMap[i][j] = deadChar;
						} else if(aliveNeighbours == 2 || aliveNeighbours == 3) {
							newMap[i][j] = aliveChar;
						} else if(aliveNeighbours > 3){
							newMap[i][j] = deadChar;
						}
					} else {
						if(aliveNeighbours == 3) {
							newMap[i][j] = aliveChar;
						} else {
							newMap[i][j] = deadChar;
						}
					}
			}
		}
		map = newMap;
	}
	
	public boolean isAlive(char c) {
		return c == aliveChar;
	}
	
	public char getTopLeftOf(int x, int y) {
		x = x - 1;
		y = y - 1;
		if(x < 0) x = lastColIndex;
		if(y < 0) y = lastRowIndex;
		return map[y][x];
	}
	
	public char getTopOf(int x, int y) {
		y = y - 1;
		if(y < 0) y = lastRowIndex;
		return map[y][x];
	}
	
	public char getTopRightOf(int x, int y) {
		x = x + 1;
		y = y - 1;
		if(x > lastColIndex) x = 0;
		if(y < 0) y = lastRowIndex;
		return map[y][x];
	}
	
	public char getRightOf(int x, int y) {
		x = x + 1;
		if(x > lastColIndex) x = 0;
		return map[y][x];
	}
	
	public char getBotRightOf(int x, int y) {
		x = x + 1;
		y = y + 1;
		if(x > lastColIndex) x = 0;
		if(y > lastRowIndex) y = 0;
		return map[y][x];
	}
	
	public char getBotOf(int x, int y) {
		y = y + 1;
		if(y > lastRowIndex) y = 0;
		return map[y][x];
	}
	
	public char getBotLeftOf(int x, int y) {
		x = x - 1;
		y = y + 1;
		if(x < 0) x = lastColIndex;
		if(y > lastRowIndex) y = 0;
		return map[y][x];
	}
	
	public char getLeftOf(int x, int y) {
		x = x - 1;
		if(x < 0) x = lastColIndex;
		return map[y][x];
	}
}
