import java.util.ArrayList;
import java.util.HashMap;

public class ParameterParser {
	
	private String[] params;
	private HashMap<String, String> optionsMap;
	private ArrayList<String> argumentsList;
	
	public ParameterParser(String[] params) {
		this.params = params;
		initialize();
		parseParams();
	}
	
	/*
	 * Initialize needed variables.
	 */
	private void initialize() {
		optionsMap = new HashMap<String, String>();
		argumentsList = new ArrayList<String>();
	}
	
	/*
	 * Parse parameter values into options and arguments.
	 */
	private void parseParams() {
		for(int i = 0; i < params.length; i++) {
			String param = params[i];
			if(param.indexOf("--") == 0) {
				optionsMap.put(param.substring(2), "");
			} else if(param.indexOf("-") == 0) {
				int j = i + 1;
				String value = params[j];
				optionsMap.put(param.substring(1), value);
				i++;
			} else {
				argumentsList.add(param);
			}
		}
	}
	
	public Boolean hasOption(String opt) {
		return optionsMap.containsKey(opt);
	}
	
	public String getOptionValue(String opt) {
		if(!hasOption(opt)) return null;
		else return optionsMap.get(opt);
	}
	
	public Boolean hasArgIndex(int index) {
		return index <= argumentsList.size() - 1;
	}
	
	public String getArgAtIndex(int index) {
		if(!hasArgIndex(index)) return null;
		return argumentsList.get(index);
	}
	
}
