**Author** : Vitaliy Karyuk
**Date**   : Feb. 22, 2016
**Notes**  : This is something I put together really quickly for fun.

This is a demonstration of the cellular automaton called the "Game of Life" which was
invented by Cambridge mathematician John Conway. Note that this is a simple demonstration
program meant to be run in a terminal window.

The program is given an input "map" with an initial state of lit/unlit cells denoted
as "@" and "O". The program then continuously computes steps either forever or until a
given limit is reached. Each step is printed every 100 milliseconds by default.

When compiling keep in mind Main.java is the main file which brings everything together.

The only mandatory parameter is the input file *.txt which must be given as the first
parameter. There are two optional parameters:
"-turns #"        :    [Compute only the first "#" states, where # is an integer > 0.
		        If this parameter is not given the program simply computes states
	                forever.]	   
"-rate #"	  :    [Changes the interval between prints to "#" milliseconds. This
		        is set to 100 milliseconds by default. # must be >= 0.]

Sample parameter input strings (make sure you enter the parameters without quotes):
	"map1.txt -turns 50 -rate 1000"
	This will run the program with input file map1.txt and compute the first 50 states,
	using a one second interval between prints.
	
	"map1.txt -rate 500"
	This will run the program with input file map1.txt forever,
	using a 0.5 second interval between prints.
	
	"map1.txt -turns 5"
	This will run the program with input file map1.txt and compute the first 5 states,
	using the default 100 millisecond interval between prints.
	
	"map1.txt"
	This will run the program with input file map1.txt forever,
	using the default 100 millisecond interval between prints

Sample input file (map1.txt):
OOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOO
OOOOOOO@OOOOOOOOOOOO
OOOOOOOO@OOOOOOOOOOO
OOOOOO@@@OOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOO

Sample output (with "map1.txt -turns 3 -rate 0"):
Turn 1:
OOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOO
OOOOOOO@OOOOOOOOOOOO
OOOOOOOO@OOOOOOOOOOO
OOOOOO@@@OOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOO
Turn 2:
OOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOO
OOOOOO@O@OOOOOOOOOOO
OOOOOOO@@OOOOOOOOOOO
OOOOOOO@OOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOO
Turn 3:
OOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOO
OOOOOOOO@OOOOOOOOOOO
OOOOOO@O@OOOOOOOOOOO
OOOOOOO@@OOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOO